package me.nikx.dctower;

/**
 * Is used to give information about the direction an elevator needs to move (or is currently moving).
 */
public enum ElevationDirection {
    UP(1),
    DOWN(-1);

    /**
     * Returns the direction the elevator needs to move to reach the {@code targetFloor} from {@code currentFloor}.
     */
    public static ElevationDirection forFloors(final int currentFloor, final int targetFloor) {
        if (currentFloor < targetFloor) {
            return ElevationDirection.UP;
        }
        if (currentFloor > targetFloor) {
            return ElevationDirection.DOWN;
        }
        throw new IllegalArgumentException("there is no elevation direction for equal floors");
    }
    private final int floorChangeOnMove;

    ElevationDirection(final int floorChangeOnMove) {
        this.floorChangeOnMove = floorChangeOnMove;
    }

    /**
     * Returns either {@code 1} or {@code -1} depending on whether the direction is up or down. Used in the elevator
     * to simplify the moving logic.
     */
    public int floorChangeOnMove() {
        return this.floorChangeOnMove;
    }

    /**
     * Returns true if the elevator can first reach {@code firstFloor} and then {@code secondFloor} when using in this
     * direction. Used to check if the elevator already passed the firstFloor and would therefor need to change its
     * direction to reach both of them.
     */
    boolean canFloorsBeReachedInOrder(final int firstFloor, final int secondFloor) {
        switch (this) {
            case UP -> {
                return firstFloor < secondFloor;
            }
            case DOWN -> {
                return firstFloor > secondFloor;
            }
        }
        return false;
    }
}
