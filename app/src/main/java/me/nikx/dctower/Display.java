package me.nikx.dctower;

import java.util.Queue;

// TODO
/**
 * In practice, this would be a separate program running on the Displays located at each floor. We would probably also not
 * use Java, and rather a Microcontroller and therefor a language targeting a lower level. The communication between the
 * panels and the tower could happen in various ways. A simple idea would be to send network requests to a central
 * server which coordinates the elevators.
 * <p>
 * For the purpose of this task the Display communicates with the server via a queue.
 */
public class Display {

    private final int floor;
    private final Queue<ElevationRequest> elevationRequestQueue;

    public Display(final int floor, final Queue<ElevationRequest> elevationRequestQueue) {
        this.floor = floor;
        this.elevationRequestQueue = elevationRequestQueue;
    }

    public void requestElevation(final int targetFloor) {
        this.elevationRequestQueue.offer(new ElevationRequest(this.floor, targetFloor));
    }
}
