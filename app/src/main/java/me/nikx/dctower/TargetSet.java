package me.nikx.dctower;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Represents the movement of an elevator in one direction, with all its stops (targets).
 * <p>
 * It is possible to add new additional targets while the elevator is already moving according to this set.
 * <p>
 * Internally this class relies on the semantics of {@link NavigableSet} to automatically sort new targets in the right
 * order.
 * <p>
 * This class does not perform any kind of input validation and is to be honest not as thread safe as it should probably
 * be. It is prone to Race Conditions, ConcurrentModificationExceptions and similar errors.
 */
public class TargetSet {

    private final ElevationDirection direction;
    private final NavigableSet<Integer> targets;

    public TargetSet(final ElevationDirection direction, final Integer... initialTargets) {
        Comparator<Integer> comparator = Integer::compareTo;
        if (direction == ElevationDirection.DOWN) {
            comparator = comparator.reversed();
        }

        this.direction = direction;
        this.targets = new TreeSet<>(comparator);

        this.targets.addAll(Arrays.asList(initialTargets));
    }

    public ElevationDirection direction() {
        return this.direction;
    }

    public int nextTarget() {
        return this.targets.iterator().next();
    }

    public void add(final int target) {
        this.targets.add(target);
    }

    public void remove(int target) {
        this.targets.remove(target);
    }

    public boolean hasTargetsLeft() {
        return !this.targets.isEmpty();
    }

    public int targetCount() {
        return this.targets.size();
    }

    @Override
    public String toString() {
        return "targets(%s : %s)".formatted(this.direction, this.targets.stream()
                .map(Object::toString)
                .collect(Collectors.joining("->")));
    }
}
