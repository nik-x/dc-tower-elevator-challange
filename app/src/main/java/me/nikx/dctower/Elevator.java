package me.nikx.dctower;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Models an elevator in the Tower.
 * <p>
 * The movement of the elevator happens stepwise, kinda like it would teleport between the individual floors, though
 * no floor is skipped.
 * <p>
 * A queue of {@link TargetSet} elements is used for the elevators target list. Each set represents the movement of
 * the elevator in one direction and can be expanded with additional stops while the elevator is already moving. If no
 * element is present in the queue, the elevator blocks until a new element comes available.
 */
public class Elevator implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Elevator.class);

    private final char identifier;

    private final BlockingQueue<TargetSet> targetQueue;

    // TODO check if volatile is needed
    private int currentFloor = 0;
    private int previousTarget = 0;

    private TargetSet currentTargetSet = null;

    private boolean active = true;

    public Elevator(final char identifier) {
        this.identifier = identifier;

        this.targetQueue = new LinkedBlockingQueue<>();
    }

    public char identifier() {
        return this.identifier;
    }

    /**
     * Stops the elevator after it finished the current target set.
     */
    public void deactivate() {
        this.active = false;

        // in case the elevator is currently blocking on the deque, this will release it
        this.targetQueue.add(new TargetSet(ElevationDirection.DOWN, 0));
    }

    public BlockingQueue<TargetSet> targetSetQueue() {
        return this.targetQueue;
    }

    /**
     * The current target set or null if this elevator is currently not moving.
     */
    public TargetSet currentTargetSet() {
        return this.currentTargetSet;
    }

    public int currentFloor() {
        return this.currentFloor;
    }

    @Override
    public void run() {
        while (this.active) {
            try {
                this.currentTargetSet = this.targetQueue.take();

                logger.info("got activated, next target is '{}'", this.currentTargetSet.nextTarget());

                // execute this set until every target was visited
                while (this.currentTargetSet.hasTargetsLeft()) {
                    // simulate elevator movement
                    Thread.sleep(50);

                    // recheck the target after each movement
                    final int nextTarget = this.currentTargetSet.nextTarget();

                    if (nextTarget != this.previousTarget) {
                        logger.debug("target set changed (previous target: {}), current set: '{}'", this.previousTarget, this.currentTargetSet);
                        this.previousTarget = nextTarget;
                    }

                    if (this.currentFloor == nextTarget) {
                        logger.debug("arrived at next target: {}", nextTarget);

                        this.currentTargetSet.remove(nextTarget);

                        // simulate elevator stop
                        Thread.sleep(100);

                        continue;
                    }

                    this.currentFloor += ElevationDirection.forFloors(this.currentFloor, nextTarget)
                            .floorChangeOnMove();
                }

                this.currentTargetSet = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
