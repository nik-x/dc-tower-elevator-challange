### Running

`./gradlew run`

### Architecture Overview

Das Programm besteht im Grunde aus 3 Teilen:

1. Displays: repräsentieren den Ort, wo man einen Aufzug anfordern kann. Werden nur beim Testen genutzt. Der
Hauptgrund warum es dafür eine eigene Klasse gibt ist, dass ich dort nen Platz hatte um ein paar meiner Gedanken über
diese Displays aufzuschreiben.
2. TowerController: dient als der zentrale Verwaltungsserver, welcher die Aufzugsanfragen verwaltet und
auf die verschiedenen Aufzüge aufteilt. Die Logik zur Aufteilung der Anfragen erlaubt Zwischenstops bei den einzelnen
Aufzügen. Mehr Informationen über den Algorithmus sind in der entsprechenden Klasse dokumentiert.
4. Aufzüge: benutzen `Thread.sleep()` um Bewegungen zu simulieren.

Jede dieser Komponenten läuft in einem eigenen Thread und die Kommunikation zwischen den einzelnen Threads erfolgt über
Queues. Die Displays werden im Main Thread zum Testen verwendet, der Controller und die Aufzüge laufen je in separaten 
Threads.
